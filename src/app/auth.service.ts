import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { User } from './interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user:Observable<User | null>;
  uid:string;

  login(email:string, password:string){
  return this.afAuth.signInWithEmailAndPassword(email,password)
  }
  
  setUid(uid){
    this.uid = uid;
  }

  getUid(){
    if(!this.uid){
      this.uid = localStorage.getItem('uid'); 
    }
    return this.uid;
  }
  logout(){
    this.afAuth.signOut();
  }

  getUser(): Observable<User | null> {
    return this.user;
  }


  // getBlogPosts():Observable<BlogPost[]>{
  //   return this.http.get<BlogPostRaw[]>("http://jsonplaceholder.typicode.com/users").pipe(
  //     map(data => this.transformPostData(data)),
  //     catchError(this.handleError)
  //   )
  // }

  register(email:string,password:string){
     return this.afAuth.createUserWithEmailAndPassword(email,password);
  }

  constructor(private afAuth:AngularFireAuth, private router:Router) {
    this.user = this.afAuth.authState;
  }
}
