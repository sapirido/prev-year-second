import { Observable } from 'rxjs';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { RegisteredUser } from '../interfaces/registered-user';

@Component({
  selector: 'app-registered-users',
  templateUrl: './registered-users.component.html',
  styleUrls: ['./registered-users.component.css']
})
export class RegisteredUsersComponent implements OnInit {

  registeredUser$:Observable<any>;
  registeredUsers:RegisteredUser[] = [];
  constructor(private auth:AuthService,private userService:UsersService) { }

  ngOnInit(): void {
    this.auth.getUser().subscribe(
      user =>{
        this.registeredUser$ = this.userService.getRegisterUsersBy(user.uid);
        this.registeredUser$.subscribe(
          docs =>{
            this.registeredUsers = [];
            for(let document of docs ){
              const registeredUser:RegisteredUser = document.payload.doc.data();
              registeredUser.uid = document.payload.doc.id;
              this.registeredUsers.push(registeredUser);
            }
          }
        )
      }
    )
  }

}
