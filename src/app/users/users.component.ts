import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { UserData } from '../interfaces/user-data';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})


export class UsersComponent implements OnInit {
  users:UserData[] = [];
  users$:Observable<UserData[]>;
  panelOpenState:boolean = false;
  userId:string;
  password:string = '12345678';
  constructor(private auth:AuthService,private userService:UsersService) { }

  addUser(email:string){
    this.auth.register(email,this.password);
  }

  createUser(user){
    console.log(this.userId);
    this.userService.createUser(this.auth.uid,user);
  }

  handleCreateUser(user){
    this.addUser(user.email);
    this.createUser(user);
  }

  ngOnInit(): void {
    this.userId = this.auth.getUid();
    this.users$ = this.userService.getUsers();
    this.users$.subscribe(
      users => this.users = users
    )
  }

}
