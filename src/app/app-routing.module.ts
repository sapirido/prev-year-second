import { RegisteredUsersComponent } from './registered-users/registered-users.component';
import { UsersComponent } from './users/users.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LoginSuccessfulComponent } from './login-successful/login-successful.component';

const routes: Routes = [
  {path: 'login', component:LoginComponent},
  {path: 'login/success', component: LoginSuccessfulComponent},
  {path:'',component:WelcomeComponent},
  {path:'users',component:UsersComponent},
  {path:'users/registered',component:RegisteredUsersComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
