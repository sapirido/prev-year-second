export interface RegisteredUser {
    uid:string;
    id:number;
    email:string;
}
