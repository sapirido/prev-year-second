import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
  password:string;
  ErrorMessage:string;

  onSubmit(){
    this.auth.login(this.email,this.password)
    .then(res => {
      localStorage.setItem('uid',res.user.uid);
      this.auth.setUid(res.user.uid);
      this.router.navigate(['login/success']);
      
      console.log(res);
      })
    .catch(err => {
      console.log(err);
      this.ErrorMessage = err.message; 
      })
  }

  clearError(){
    this.ErrorMessage = '';
  }

  constructor(public auth:AuthService,public router:Router) { }

  ngOnInit(): void {
  }

}
