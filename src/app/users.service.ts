import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { RegisteredUser } from './interfaces/registered-user';
import { UserData } from './interfaces/user-data';
import { UserRaw } from './interfaces/user-raw';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
URL:string  ='http://jsonplaceholder.typicode.com/users'; 

userCollection:AngularFirestoreCollection = this.db.collection('users');
registeredCollection:AngularFirestoreCollection;
  constructor(private http:HttpClient,private db:AngularFirestore) { }

  getUsers():Observable<UserData[]>{
    return this.http.get<UserRaw[]>(this.URL).pipe(
      map(data => this.transformuserData(data),
      catchError(this.handleError)
      )
    )
  }

  createUser(userId,user:any):Observable<any>{
   this.registeredCollection = this.userCollection.doc(userId).collection('registered_users');
   this.registeredCollection.add(user);
    return this.registeredCollection.snapshotChanges();
  }

  getRegisterUsersBy(userId:string):Observable<any[]>{
    this.registeredCollection = this.userCollection.doc(userId).collection('registered_users');
    return this.registeredCollection.snapshotChanges();
  }
  transformuserData(data:UserRaw[]):UserData[]{
    const users:UserData[] =  data.map(raw => {
    return{
       id:raw.id,
       name:raw.name,
       email:raw.email,
       username:raw.username,
       phone:raw.phone,
       website:raw.website,
       company:{
        name:raw.company.name,
        catchPhrase:raw.company.catchPhrase,
        bs:raw.company.bs
       }
     }
   })
 
   return users;
   }

  public handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'Server error');
   }
 
}
