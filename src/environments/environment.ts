// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBMk6Z10twI-NwIo2TIop7Xu-Laluuqv0o",
    authDomain: "prev-year-second.firebaseapp.com",
    projectId: "prev-year-second",
    storageBucket: "prev-year-second.appspot.com",
    messagingSenderId: "496152994867",
    appId: "1:496152994867:web:9053ad1de7ac968073af46"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
